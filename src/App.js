import React from "react";
import {
  AmplifyAuthenticator,
  AmplifySignUp,
  AmplifySignOut,
  AmplifySignIn,
} from "@aws-amplify/ui-react";
import "@aws-amplify/ui/dist/style.css";

import ImageViewer from "./ImageViewer";

const App = () => {
  return (
    <AmplifyAuthenticator>
      <AmplifySignUp
        formFields={[
          {
            required: true,
            type: "email",
          },
          {
            required: true,
            type: "password",
          },
        ]}
        usernameAlias={"email"}
        slot="sign-up"
      ></AmplifySignUp>
      <AmplifySignIn usernameAlias={"email"} slot="sign-in"></AmplifySignIn>
      <div>
        <AmplifySignOut />
        <ImageViewer />
      </div>
    </AmplifyAuthenticator>
  );
};

export default App;
