/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createObject = /* GraphQL */ `
  mutation CreateObject(
    $input: CreateObjectInput!
    $condition: ModelObjectConditionInput
  ) {
    createObject(input: $input, condition: $condition) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
export const updateObject = /* GraphQL */ `
  mutation UpdateObject(
    $input: UpdateObjectInput!
    $condition: ModelObjectConditionInput
  ) {
    updateObject(input: $input, condition: $condition) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
export const deleteObject = /* GraphQL */ `
  mutation DeleteObject(
    $input: DeleteObjectInput!
    $condition: ModelObjectConditionInput
  ) {
    deleteObject(input: $input, condition: $condition) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
