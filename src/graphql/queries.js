/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getObject = /* GraphQL */ `
  query GetObject($id: ID!) {
    getObject(id: $id) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
export const listObjects = /* GraphQL */ `
  query ListObjects(
    $id: ID
    $filter: ModelObjectFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listObjects(
      id: $id
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        status
        databaseKey
        meta
        label
        points {
          x
          y
          label
        }
        image {
          bucket
          region
          key
        }
        owner
      }
      nextToken
    }
  }
`;
