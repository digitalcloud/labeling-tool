/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateObject = /* GraphQL */ `
  subscription OnCreateObject($owner: String) {
    onCreateObject(owner: $owner) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
export const onUpdateObject = /* GraphQL */ `
  subscription OnUpdateObject($owner: String) {
    onUpdateObject(owner: $owner) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
export const onDeleteObject = /* GraphQL */ `
  subscription OnDeleteObject($owner: String) {
    onDeleteObject(owner: $owner) {
      id
      status
      databaseKey
      meta
      label
      points {
        x
        y
        label
      }
      image {
        bucket
        region
        key
      }
      owner
    }
  }
`;
