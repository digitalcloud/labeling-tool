import React from "react";
import ZoomableImage from "react-zoomable-image";
import { listObjects } from "./graphql/queries";
import { useQuery } from "./utils";
import { Storage } from "aws-amplify";

const ImageViewer = () => {
  const { data } = useQuery(listObjects);

  return (
    <div>
        <ZoomableImage
          baseImage={{
            alt: "An image",
            src: "https://labelingtoole81cbe1b344248cca93ed100a5613284102656-production.s3.eu-central-1.amazonaws.com/public/ISIC_0000001.jpg",
            width: 1022,
            height: 767,
          }}
          largeImage={{
            alt: "A large image",
            src: "https://labelingtoole81cbe1b344248cca93ed100a5613284102656-production.s3.eu-central-1.amazonaws.com/public/ISIC_0000001.jpg",
            width: 1022,
            height: 767,
          }}
          zoomTransitionTime={100}
          displayMap={false}
        />
    </div>
  );
};

export default ImageViewer;
