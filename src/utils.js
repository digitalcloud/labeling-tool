import React from "react";
import { API, graphqlOperation } from 'aws-amplify'

  
export const useQuery = (
    query, variables
  ) => {
    const [loading, setLoading] = React.useState(true);
    const [error, setError] = React.useState('');
    const [data, setData] = React.useState({});
  
    const fetchQuery = async (query, variables) => {
      try {
        const { data } = (await API.graphql(
          graphqlOperation(query, variables)
        ))
        setData(data);
      } catch (error) {
        console.log(error);
        setError(error);
      } finally {
        setLoading(false);
      }
    };
  
    const refetch = () => {
      fetchQuery(query, variables);
    };
  
    React.useEffect(() => {
      fetchQuery(query, variables);
    }, [query, variables]);
  
    return {
      loading,
      data,
      error,
      refetch,
    };
  };